package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.kazuhiroserizawa.mystudy.R;

import java.util.ArrayList;
import java.util.List;

public class ListSubActivity extends ListActivity {
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> data = new ArrayList<String>();
        data.add("千歳");
        data.add("千代田");
        data.add("飛鷹");
        data.add("準鷹");
        data.add("瑞鳳");
        data.add("祥鳳");
        data.add("天龍");
        data.add("龍田");
        data.add("阿賀野");
        adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, data);
        setListAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        adapter.remove((String) ((TextView) v).getText());
    }
}
