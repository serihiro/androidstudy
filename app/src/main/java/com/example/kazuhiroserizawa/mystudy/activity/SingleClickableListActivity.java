package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kazuhiroserizawa.mystudy.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SingleClickableListActivity extends Activity {

    private ArrayAdapter listAdapter;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_clickable_list);
        generateSingleClickableListView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_clickable_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void generateSingleClickableListView() {
        List data = new ArrayList<String>(Arrays.asList("赤城", "加賀", "飛龍", "蒼龍",
                "瑞鶴", "翔鶴", "金剛", "青葉", "不知火"));
        listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_expandable_list_item_1,
                data);
        ListView listView = (ListView) findViewById(R.id.single_clickable_list);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position,
                                            long id) {
                        Log.d("debug", "clicked");
                        listAdapter.remove((String) ((TextView) view).getText());
                    }
                }
        );

        listView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {

                    @Override public boolean onItemLongClick(AdapterView<?> parent,
                                                             View view,
                                                             int position, long id) {
                        Log.d("debug", "longClicked");
                        Toast.makeText(context,
                                (String) ((TextView) view).getText(),
                                Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
        );
    }
}
