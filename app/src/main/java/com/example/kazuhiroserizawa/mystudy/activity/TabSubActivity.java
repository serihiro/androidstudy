package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.TabActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

import com.example.kazuhiroserizawa.mystudy.R;

public class TabSubActivity extends TabActivity {


    private TabHost host;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        host = getTabHost();
        host.setup();

        setupTab("Tab1", R.id.tab1, R.string.tab1, R.drawable.tab_icon);
        setupTab("Tab2", R.id.tab2, R.string.tab2, R.drawable.tab_icon);
        setupTab("Tab3", R.id.tab3, R.string.tab3, R.drawable.tab_icon);

        host.setCurrentTab(0);
    }

    private void setupTab(String tabName, int tabResourceId,
                          int tabStringResourceId, int tabIconId) {
        TabHost.TabSpec tab = host.newTabSpec(tabName);
        tab.setIndicator(
                getResources().getString(tabStringResourceId),
                getResources().getDrawable(tabIconId)
        );
        tab.setContent(tabResourceId);
        host.addTab(tab);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tab_sub, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
