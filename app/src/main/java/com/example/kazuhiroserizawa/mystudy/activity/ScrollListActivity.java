package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.kazuhiroserizawa.mystudy.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScrollListActivity extends Activity {

    private ArrayAdapter listAdapter;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_list);
        generateList();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.scroll_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void generateList() {
        List data = new ArrayList<String>(Arrays.asList("赤城", "加賀", "飛龍", "蒼龍",
                "瑞鶴", "翔鶴", "金剛", "青葉", "不知火", "a", "b", "c", "d", "e", "f", "g"));
        listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                data);
        ListView listView = (ListView) findViewById(R.id.scroll_list);
        listView.setAdapter(listAdapter);

        listView.setOnScrollListener(
                new AbsListView.OnScrollListener() {
                    @Override public void onScrollStateChanged(AbsListView view,
                                                               int scrollState) {
                    }

                    @Override public void onScroll(AbsListView view, int firstVisibleItem,
                                                   int visibleItemCount,
                                                   int totalItemCount) {
                        Log.d("debug", "onScroll");
                        Log.d("totalItemCount", String.valueOf(totalItemCount));

                        if (firstVisibleItem + visibleItemCount + 3 == totalItemCount) {
                            listAdapter.add("龍田");
                            listAdapter.add("天龍");
                            listAdapter.add("羽黒");
                            listAdapter.add("陽炎");
                            listAdapter.add("比叡");
                            listAdapter.add("龍田");
                            listAdapter.add("天龍");
                            listAdapter.add("羽黒");
                            listAdapter.add("陽炎");
                            listAdapter.add("比叡");
                            listAdapter.add("龍田");
                            listAdapter.add("天龍");
                            listAdapter.add("羽黒");
                            listAdapter.add("陽炎");
                            listAdapter.add("比叡");
                            listAdapter.add("龍田");
                            listAdapter.add("天龍");
                            listAdapter.add("羽黒");
                            listAdapter.add("陽炎");
                            listAdapter.add("比叡");
                        }

                    }
                }
        );
    }
}
