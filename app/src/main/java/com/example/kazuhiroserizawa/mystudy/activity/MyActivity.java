package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kazuhiroserizawa.mystudy.R;
import com.example.kazuhiroserizawa.mystudy.fragment.AnotherFragment;

import java.util.Date;


public class MyActivity extends Activity
        implements AnotherFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // レイアウトを指定して、Activity がコントロールする View として扱うようにする
        setContentView(R.layout.activity_my);
        final TextView txtView = (TextView) findViewById(R.id.MyTextView);
        Button currentTimeButton = (Button) findViewById(R.id.currentTimeButton);
        currentTimeButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtView.setText(new Date().toString());
                    }
                }
        );

        Button anotherButton = (Button) findViewById(R.id.btn_another);
        anotherButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        TextView txtResult = (TextView) findViewById(R.id.MyTextView);
        //mapに詰める
        bundle.putString("txtResult", txtResult.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        TextView view = (TextView) findViewById(R.id.MyTextView);
        //mapから取り出してviewにセットする
        view.setText(bundle.getString("txtResult"));
    }


    public void currentTimeButton_onClick(View view) {
        TextView text = (TextView) findViewById(R.id.MyTextView);
        text.setText(new Date().toString());

        Toast.makeText(this, new Date().toString(), Toast.LENGTH_LONG).show();

        Log.d("CurrentTime", new Date().toString());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
