package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.kazuhiroserizawa.mystudy.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomLayoutListViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_layout_list_view);
        String titles[] = {"革命のエチュード", "G線上のアリア", "シャコンヌ", "夜の女王のアリア", "春の海"};
        String tags[] = {"ピアノ", "バイオリン", "チェロ", "声楽", "箏"};
        String descs[] = {"ピアノの詩人と言われたショパンの代表的なピアノ曲です。",
                "バッハの作品。バイオリンの二線のみで演奏できることからこのタイトルで親しまれています。",
                "バッハの作品。パルティータ第二番の終曲です。",
                "モーツァルト作曲のオペラ「魔笛」",
                "宮城道雄の作品です。曲の舞台は鞍の浦と言われています。"
        };

        List<HashMap<String, String>> data =
                new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < titles.length; i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put("title", titles[i]);
            item.put("tag", tags[i]);
            item.put("desc", descs[i]);
            data.add(item);
        }

        SimpleAdapter adapter =
                new SimpleAdapter(this, data, R.layout.item_music_list,
                        new String[]{"title", "tag", "desc"},
                        new int[]{R.id.title, R.id.tag, R.id.desc}
                );

        ListView listView = (ListView) findViewById(R.id.custom_layout_list);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_layout_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
