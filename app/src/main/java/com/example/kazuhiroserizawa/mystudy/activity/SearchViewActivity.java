package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.kazuhiroserizawa.mystudy.R;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);

        List data = new ArrayList<String>(Arrays.asList("赤城", "加賀", "飛龍", "蒼龍",
                "瑞鶴", "翔鶴", "金剛", "青葉", "不知火", "a", "b", "c", "d", "e", "f", "g"));
        ArrayAdapter listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                data);
        final ListView listView = (ListView) findViewById(R.id.search_view_list);
        listView.setAdapter(listAdapter);
        listView.setTextFilterEnabled(true);

        SearchView searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override public boolean onQueryTextChange(String query) {
                if (StringUtils.isEmpty(query)) {
                    listView.clearTextFilter();
                } else {
                    listView.setFilterText(query);
                }
                return false;
            }

            @Override public boolean onQueryTextSubmit(String newText) {
                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
