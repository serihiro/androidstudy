package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.kazuhiroserizawa.mystudy.R;
import com.example.kazuhiroserizawa.mystudy.fragment.AnotherFragment;

public class AnotherActivity extends Activity
        implements AnotherFragment.OnFragmentInteractionListener {

    private final AnotherActivity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        bindLaunchActivity((Button) findViewById(R.id.button_myactivity),
                MyActivity.class);

        bindLaunchActivity((Button) findViewById(R.id.button_text_form_activity),
                TextFormActivity.class);

        bindLaunchActivity((Button) findViewById(R.id.checkbox_activity),
                CheckBoxActivity.class);

        bindLaunchActivity((Button) findViewById(R.id.list_view__activity),
                ListViewActivity.class);

        bindLaunchActivity((Button) findViewById(R.id.list_view_next_activity),
                ListViewNextActivity.class);

        bindLaunchActivity((Button) findViewById(R.id.single_clickable_list_activity),
                SingleClickableListActivity.class);

        bindLaunchActivity(
                (Button) findViewById(R.id.single_selectable_list_view_activity),
                SingleSelectableListViewActivity.class);

        bindLaunchActivity(
                (Button) findViewById(R.id.scroll_list_activity)
                , ScrollListActivity.class);

        bindLaunchActivity(
                (Button) findViewById(R.id.custom_layout_list_activity),
                CustomLayoutListViewActivity.class);

        bindLaunchActivity(
                (Button) findViewById(R.id.list_activity),
                ListSubActivity.class);

        bindLaunchActivity(
                (Button) findViewById(R.id.search_view),
                SearchViewActivity.class);

        bindLaunchActivity(
                (Button) findViewById(R.id.tab_activity),
                TabSubActivity.class);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.another, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * 指定されたボタンクリックでActivityが起動するようにbindする
     *
     * @param button
     * @param clazz  extends Activity
     */
    private void bindLaunchActivity(Button button,
                                    final Class<? extends Activity> clazz) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent i = new Intent(context, clazz);
                startActivity(i);
            }
        });
    }
}
