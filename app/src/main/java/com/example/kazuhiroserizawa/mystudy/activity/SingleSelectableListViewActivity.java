package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kazuhiroserizawa.mystudy.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SingleSelectableListViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_selectable_list_view);
        generateSingleSelectableListView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_selectable_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void generateSingleSelectableListView() {
        List data = new ArrayList<String>(Arrays.asList("赤城", "加賀", "飛龍", "蒼龍",
                "瑞鶴", "翔鶴", "金剛", "青葉", "不知火"));
        ArrayAdapter listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_single_choice,
                data);
        ListView listView = (ListView) findViewById(R.id.single_selectable_list);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view,
                                              int position,
                                              long id) {
                CharSequence text = ((TextView) view).getText();
                Toast.makeText(SingleSelectableListViewActivity.this,
                        String.format("選択したのは%s", text.toString()),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
