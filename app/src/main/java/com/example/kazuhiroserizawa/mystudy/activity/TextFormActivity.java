package com.example.kazuhiroserizawa.mystudy.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kazuhiroserizawa.mystudy.R;

public class TextFormActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_form);

        Button nameSendButton = (Button) findViewById(R.id.send_button);
        final EditText nameEdit = (EditText) findViewById(R.id.name_edit);
        final TextView displayNameText = (TextView) findViewById(R.id.name_view);
        nameSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNameText.setText(String.format("こんにちは、%sさん！", nameEdit.getText()));
            }
        });

        setClickEvent((EditText) findViewById(R.id.password_edit),
                (TextView) findViewById(R.id.password_view),
                (Button) findViewById(R.id.password_send_button)
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.text_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setClickEvent(final TextView editView, final TextView displayView,
                               Button target) {
        target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayView.setText(editView.getText());
            }
        });
    }

}
